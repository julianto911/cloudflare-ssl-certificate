# Cloudflare SSL Certificate

How to enable Cloudflare SSL on Apache server
1. Login to your cloudflare account
2. Click menu SSL/TLS
3. Click on Origin Server tab
4. Click Create Certificate Button
5. Private key type should be RSA, validity 15 years, click Next
6. There will be pem and private key provided on the screen
7. Open console to your server, make sure you have enabled sudo a2enmod ssl
8. Create cloudflare ssl folder ex : sudo mkdir /etc/cloudflare
9. nano /etc/cloudflare/yourdomain.com.pem
10. Copy and paste the pem key string value to the nano interface. save and exit nano
11. nano /etc/cloudflare/yourdomain.com.key
12. Copy and paste the private key string value to the nano interface. save and exit nano
13. nano /etc/apache2/sites-available/yourdomain.com.conf
14. add this to your virtual host setting :


   <VirtualHost *:443>
       ServerName yourdomain.com
       ServerAlias www.yourdomain.com
       ServerAdmin youremail@dot.com
       DocumentRoot /var/www/html/yourdomain.com/public_html

       ErrorLog /var/www/html/yourdomain.com/logs/error.log
       CustomLog /var/www/html/yourdomain.com/logs/access.log combined

       SSLEngine on
       SSLCertificateFile /etc/cloudflare/yourdomain.com.pem
       SSLCertificateKeyFile /etc/cloudflare/yourdomain.com.key

   </VirtualHost>


15. save and exit nano.
16. service apache2 reload
17. Back to cloudflare screen,click Overview tab
18. Chooose Full(strict) option
19. Wait until the changes being applied by cloudflare



